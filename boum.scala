import scala.util.Random
import Array._
import javax.swing.ImageIcon
import scala.math._

class Demineur extends Jeu[MonBouton] {
   valeur_defaut = Array(9,9,10) 

   // Taille du champ de mine. Par défaut, réglé en "Facile".
   tailleX = valeur_defaut(0)
   tailleY = valeur_defaut(1)

   // Nombre de mines dans le champ. Réglé en "Facile"
   private var nb_mines = valeur_defaut(2)

   // Propage le clic pour le premier coup après non
   private var premierCoup = true 

   // Compteur du nombre de mines trouvées.
   private var m_trouvees = 0
   
   // Compteur du nombre de cases restantes.
   private var c_restantes = {tailleX*tailleY - nb_mines}
   
   // Détermine si on a gagné.
   def victoire() = {(nb_mines == m_trouvees) || c_restantes == 0}

   // Niveau de difficulté de la partie
   // 0 : Super Noob
   // 1 : Moyem Noob
   // 2 : Noob
   protected var noobitude = 0

   // Matrice représentant le champ de mines. Ses valeurs sont comprises entre 0 et 9.
   // 0-8 : Nombre de mines autour de la case vide.
   // 9 : Signale l'emplacement d'une mine sur la case.
   // Par défaut, on est réglé sur facile.
//   private var champ = ofDim[Int](tailleX,tailleY)
   
   // Matrice des boutons associés aux cases (il me semble plus pertinent de l'inclure dans le jeu).
    override  protected var boutons = ofDim[MonBouton](tailleX,tailleY)
   
   // Réinitialise les paramètres ci-dessus en cas de changement de difficulté.
   def reparametrage (x: Int, y: Int, d: Int, n: Int) = {
      tailleX = x
      tailleY = y
      nb_mines = n
      noobitude = d
      c_restantes = tailleX*tailleY - nb_mines
      boutons = ofDim[MonBouton](tailleX, tailleY)
   }
   
   // On n'aura normalement pas à utiliser cette fonction, mais elle doit exister
   def reparametrage(x: Int, y: Int, d: Int) = reparametrage(x,y,d,10) 

   // Focntion de redémarrage
   def redemarre() = {
      premierCoup = true
      for (i <- 0 to (tailleX-1)) {
         for (j <- 0 to (tailleY-1)) {
            boutons(i)(j).reset
            boutons(i)(j).icon = scala.swing.Swing.EmptyIcon
            boutons(i)(j).enabled = true
         }
      }
      m_trouvees = 0
      c_restantes = tailleX*tailleY - nb_mines
   }
   
   // Réactive la variable de premier coup
   def resetPremierCoup = premierCoup = true
   
   // (Re)crée un champ de boutons sans toucher aux valeurs
   def creation_boutons = {
      map_init((i,j) => new MonBouton(i,j), boutons)
   }

   // Initialisation basique d'un champ de mine.
   // On crée les boutons ; puis on génère nb_mines positions aléatoires, et on
   // incrémente les cases alentour pour chaque mine placée.
   def initialise = {
      creation_boutons
      var x = Random.nextInt(tailleX)
      var y = Random.nextInt(tailleY)
      for (i <- 0 to min(nb_mines-1,tailleX*tailleY-1)) {
         while(boutons(x)(y).getContenu == 9) {
            x = Random.nextInt(tailleX)
            y = Random.nextInt(tailleY)
         }
         boutons(x)(y).setContenu(9)
         if (x != 0) { 
            if (!boutons(x-1)(y).mines) { boutons(x-1)(y).incrContenu}
         }
         if (x != (tailleX-1)) { 
            if (!boutons(x+1)(y).mines) { boutons(x+1)(y).incrContenu} 
         }
         if (y != 0) { 
            if (!boutons(x)(y-1).mines) { boutons(x)(y-1).incrContenu} 
         }
         if (y != (tailleY-1)) {
            if (!boutons(x)(y+1).mines) { boutons(x)(y+1).incrContenu} 
         }
         if (x != 0 && y != 0) {
             if (!boutons(x-1)(y-1).mines) { boutons(x-1)(y-1).incrContenu} 
         }
         if (x != 0 && y != (tailleY-1)) {
            if (!boutons(x-1)(y+1).mines) { boutons(x-1)(y+1).incrContenu} 
         }
         if (x != (tailleX-1) && y != 0) {
            if (!boutons(x+1)(y-1).mines) { boutons(x+1)(y-1).incrContenu} 
         }
         if (x != (tailleX-1) && y != (tailleY-1)) {
            if (!boutons(x+1)(y+1).mines) { boutons(x+1)(y+1).incrContenu} 
         }
      }
      var perdu = clique_action_gauche(0,0)
      var found = true
      var cmpt = 0
      def f(x: MonBouton)= {
         var plop = noobitude match {
            case 0 => SuperNoob(x.getTheX,x.getTheY)
            case _ => (true,false)
         }
         perdu = plop._1; found = found || plop._2}
      while(!perdu && !victoire && found && cmpt < 10000){
         found = false
         map_apply(f, boutons)
         cmpt += 1
      }
      if((perdu || !found) && noobitude == 0){initialise}
      else(redemarre())
   }

   // Même fonction, mais avec la composante "random seed" en paramètre.
   def initialise (n: Int) = {
      creation_boutons
      var ran = new Random(n)
      var x = ran.nextInt(tailleX)
      var y = ran.nextInt(tailleY)
      for (i <- 0 to min(nb_mines-1,tailleX*tailleY-1)) {
         while(boutons(x)(y).getContenu == 9) {
            x = ran.nextInt(tailleX)
            y = ran.nextInt(tailleY)
         }
         boutons(x)(y).setContenu(9)
         var voisin = voisinEtendu(x,y):::(Nil)
         for((vx,vy) <- voisin){
            if(!boutons(vx)(vy).mines) { boutons(vx)(vy).incrContenu }}
      }
      var perdu = clique_action_gauche(0,0)
      var found = true
      var cmpt = 0
      def f(x: MonBouton)= {
         var plop = noobitude match {
            case 0 => SuperNoob(x.getTheX,x.getTheY)
            case _ => (true,false)
         }
         perdu = plop._1; found = found || plop._2}
      while(!perdu && !victoire && found && cmpt < 10000){
         found = false
         map_apply(f, boutons)
         cmpt += 1
      }
      if((perdu || !found) && noobitude == 0){initialise(n+1)}
      else(redemarre())

   }

  // Propagation du dévoilement des cases vides     
  private def propage(c: Int, x: Int, y: Int){
       if(c == 0 || premierCoup){
          
          if(premierCoup){premierCoup = false}
          
          var voisin = voisinEtendu(x,y)
          for((vx,vy) <- voisin){
             if(boutons(vx)(vy).estLibre && !boutons(vx)(vy).mines){clique_action_gauche(vx,vy)}}
       }
   }
   
   def devoile (x: Int, y: Int) = {
      var perdu = false
      var nb = boutons(x)(y).getContenu
      var voisin = voisinEtendu(x,y)
      // Si unn drapeau n'a pas été posé, on n'autorise pas le dévoilement.
      for((vx,vy) <- voisin) {
         if (boutons(vx)(vy).possedeDrapeau) { nb -= 1 }
      } 

      if (nb != 0) { false } else {
        for((vx,vy) <- voisin){
           if(boutons(vx)(vy).estLibre){perdu = perdu || clique_action_gauche(vx,vy)}}
        perdu
      }
   }

   // Définit les conséquences d'un clic sur un bouton.
   def clique_action_gauche(x: Int, y: Int) : Boolean = {
      if(boutons(x)(y).estLibre){ //Cases non dévoilée, on la dévoile
          boutons(x)(y).getContenu match {
             case 0 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_zero.png"))
                boutons(x)(y).enabled = false
                boutons(x)(y).chgLibre
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_zero.png"))
                c_restantes -= 1
                propage(0,x,y)
                false
             case 1 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_un.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_un.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(1,x,y)
                false
             case 2 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_deux.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_deux.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(2,x,y)
                false
             case 3 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_trois.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_trois.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(3,x,y)
                false
             case 4 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_quatre.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_quatre.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(4,x,y)
                false
             case 5 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_cinq.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_cinq.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(5,x,y)
                false
             case 6 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_six.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_six.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(6,x,y)
                false
             case 7 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_sept.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_sept.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(7,x,y)
                false
             case 8 => boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_huit.png"))
                boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_huit.png"))
                boutons(x)(y).chgLibre
                c_restantes -= 1
                propage(8,x,y)
                false
             case 9 => for (i <- 0 to (tailleX-1)) {
                for (j <- 0 to (tailleY-1)) {
                   if (boutons(i)(j).mines) {
                      boutons(x)(y).chgLibre
                      boutons(i)(j).icon = new ImageIcon(getClass.getResource("case_mine.png"))
                      boutons(i)(j).enabled = false
                      boutons(i)(j).disabledIcon = new ImageIcon(getClass.getResource("case_mine.png"))
                   }
                }
             }
             true
          }
       } else {// Cases déjà dévoilée, on dévoile le tour
          devoile(x,y)
       }
   }

   // Affichage des drapeaux 
   def clique_action_droit(x: Int, y: Int) = {
      if (boutons(x)(y).enabled == true && boutons(x)(y).estLibre) {
         boutons(x)(y).icon = new ImageIcon(getClass.getResource("case_drapeau.png"))
         boutons(x)(y).enabled = false
         boutons(x)(y).chgLibre
         boutons(x)(y).chgDrapeau
         boutons(x)(y).disabledIcon = new ImageIcon(getClass.getResource("case_drapeau.png"))
         if (boutons(x)(y).mines) { m_trouvees += 1 } else { m_trouvees -= 1 }
      }
      else {
         if (boutons(x)(y).possedeDrapeau) {
            boutons(x)(y).icon = scala.swing.Swing.EmptyIcon
            boutons(x)(y).enabled = true
            boutons(x)(y).chgLibre
            boutons(x)(y).chgDrapeau
            if (boutons(x)(y).mines) { m_trouvees -= 1 } else { m_trouvees += 1 }
         }
      }
      victoire()
   }

   //solveur facile
   def SuperNoob(x : Int, y : Int)={
      var result = false
      var found = false
      var voisin = voisinEtendu(x,y)
      var count = 0
      if(!boutons(x)(y).mines){
         voisin.map(x =>
            if(boutons(x._1)(x._2).estLibre || boutons(x._1)(x._2).possedeDrapeau){
            count += 1})
         if(count == boutons(x)(y).getContenu){
            voisin.map(
            x=>if(boutons(x._1)(x._2).estLibre){
            result = clique_action_droit(x._1,x._2) || result
            found = true})}
         else{
             count = 0
             voisin.map(x => if(!boutons(x._1)(x._2).possedeDrapeau){
            count += 1})
            if(count == voisin.length - boutons(x)(y).getContenu){
               voisin.map(x=> 
               if(boutons(x._1)(x._2).estLibre){result= clique_action_gauche(x._1,x._2) || result
               found = true})}
         }
      }
      (result,found) 
   }
   
   //vérifie si des nouvelles sol sont possible
   def checkmodif(x : Int, y : Int):Unit = {
      var voisin = voisinEtendu(x,y)
      if(noobitude == 0){
         voisin.map(x=>if(boutons(x._1)(x._2).estLibre && ! boutons(x._1)(x._2).possedeDrapeau){
         if(SuperNoob(x._1,x._2)._1){checkmodif(x._1,x._2)}})
      }
//      if(noobitude == 1){
//         voisin.map(x=>if(boutons(x._1)(x._2).estLibre && ! boutons(x._1)(x._2).possedeDrapeau){
//         if(MiddleNoob(i,j)){checkmodif(i,j)}})
//
//      }
//      if(noobitude == 2){
//         voisin.map((i,j)=>if(boutons(i)(j).estLibre() && ! boutons(i)(j).possedeDrapeau()){
//         if(Noob(i,j)){checkmodif(i,j)}})}
   }
}
