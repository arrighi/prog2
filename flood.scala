import scala.util.Random
import Array._
import java.awt.Color

class Flood extends JeuCouleur {

   // curent color
   private var cc = Color.blue

   // compteur de coup
   private var compteur = 0

   def getCompteur = compteur

   // Génération d'une grille de départ aléatoire.
   // 0 blue
   // 1 green
   // 2 orange
   // 3 magenta
   // 4 red
   def initialise() = {
      map_init((i,j) => new MonBouton(i,j), boutons)
      var x = Random.nextInt(5)
      def f (b: MonBouton) = {
          var i = b.getTheX
          var j = b.getTheY 
          x match {
            case 0 => boutons(i)(j).active_couleur(Color.blue)
            case 1 => boutons(i)(j).active_couleur(Color.green)
            case 2 => boutons(i)(j).active_couleur(Color.orange)
            case 3 => boutons(i)(j).active_couleur(new Color(140,0,230))
            case 4 => boutons(i)(j).active_couleur(Color.red)
          }
          x = Random.nextInt(5)
      }
      map_apply(f, boutons)
      cc = boutons(0)(0).getCouleur
      compteur = 0
      def init (b: MonBouton) = {
         var i = b.getTheX
         var j = b.getTheY
         sauvegardeMat(i)(j) = boutons(i)(j).getCouleur
      }
      map_apply(init, boutons)
   }

   // Génération aléatoire avec graine
   def initialise(n: Int) = {
      map_init((i,j) => new MonBouton(i,j), boutons)
      var ran = new Random(n)
      var x = Random.nextInt(5)
      def f (b: MonBouton) = {
          var i = b.getTheX
          var j = b.getTheY 
          x match {
            case 0 => boutons(i)(j).active_couleur(Color.blue)
            case 1 => boutons(i)(j).active_couleur(Color.green)
            case 2 => boutons(i)(j).active_couleur(Color.orange)
            case 3 => boutons(i)(j).active_couleur(new Color(140,0,230))
            case 4 => boutons(i)(j).active_couleur(Color.red)
          }
          x = Random.nextInt(5)
      }
      map_apply(f, boutons)
      cc = boutons(0)(0).getCouleur
      compteur = 0
      def init (b: MonBouton) = {
         var i = b.getTheX
         var j = b.getTheY
         sauvegardeMat(i)(j) = boutons(i)(j).getCouleur
      }
      map_apply(init, boutons)
   }

   // Modification de la couleur de la zone principale.
   def chgColor(i: Int, j: Int, c : Color) : Unit = {
      if (boutons(i)(j).estColore(cc)) {
         boutons(i)(j).active_couleur(c)
         for((x,y) <- voisinDirect(i,j))
            chgColor(x,y,c)
      }
   }
   
   // Renvoire true si toutes les cases du terrain sont blanches
   def victoire = {
      var res = true
      def f (b : MonBouton) = {
           var i = b.getTheX
           var j = b.getTheY
           res = (res && boutons(i)(j).estColore(boutons(0)(0).getCouleur))
      }
      map_apply(f, boutons)
      res
   }
   
   // Conséquence du clic d'un bouton.
   def clique_action_gauche(i: Int, j:Int) : Boolean = {
     if(boutons(i)(j).getCouleur != cc){ 
         chgColor(0,0,boutons(i)(j).getCouleur)
         cc = boutons(i)(j).getCouleur
         compteur += 1
      }
      false 
   }
   
}
