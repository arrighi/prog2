import scala.util.Random
import Array._
import java.awt.Color

class Flip extends JeuCouleur {

   // Génération d'une grille de départ aléatoire.
   def initialise() = {
      map_init((i,j) => new MonBouton(i,j), boutons)
      def coloriage (b: MonBouton) = {
         b.active_couleur(Color.white)
      }
      map_apply(coloriage, boutons)
      var x = Random.nextBoolean
      def f (b: MonBouton) = {
          var i = b.getTheX
          var j = b.getTheY 
          if (x) { var bl = clique_action_gauche(i,j) }
          x = Random.nextBoolean 
      }
      map_apply(f, boutons)
      def init (b: MonBouton) = {
         var i = b.getTheX
         var j = b.getTheY
         sauvegardeMat(i)(j) = boutons(i)(j).getCouleur
      }
      map_apply(init, boutons)
   }

   // Génération aléatoire avec graine
   def initialise(n: Int) = {
      map_init((i,j) => new MonBouton(i,j), boutons)
      def coloriage (b: MonBouton) = {
         b.active_couleur(Color.white)
      }
      map_apply(coloriage, boutons)
      var ran = new Random(n)
      var x = ran.nextBoolean
      def f (b: MonBouton) = {
         var i = b.getTheX
         var j = b.getTheY 
         if (x) { var bl = clique_action_gauche(i,j) } 
         x = ran.nextBoolean
      }
      map_apply(f, boutons)
      def init (b: MonBouton) = {
         var i = b.getTheX
         var j = b.getTheY
         sauvegardeMat(i)(j) = boutons(i)(j).getCouleur
      }
      map_apply(init, boutons)
   }

   // Modification de la couleur d'un bouton.
   def chgBouton(i: Int, j: Int) = {
      if (boutons(i)(j).estColore(Color.white)) {
         boutons(i)(j).active_couleur(Color.black)
      } else {
         boutons(i)(j).active_couleur(Color.white)
      }
   }
   
   // Renvoire true si toutes les cases du terrain sont blanches
   def victoire = {
      var res = true
      def f (b : MonBouton) = {
           var i = b.getTheX
           var j = b.getTheY
           res = (res && boutons(i)(j).estColore(Color.white))
      }
      map_apply(f, boutons)
      res
   }
   
   // Conséquence du clic d'un bouton.
   def clique_action_gauche(i: Int, j:Int) : Boolean = {
      chgBouton(i,j)
      var voisin = voisinDirect(i,j)
      for((x,y) <- voisin){chgBouton(x,y)}
      false 
   }

}
