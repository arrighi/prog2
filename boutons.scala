import swing._
import java.awt.Color
import javax.swing.ImageIcon

class MonBouton(protected val x : Int, protected val y : Int) extends Button {
    preferredSize = new Dimension(50, 50)
    borderPainted = true
    
    def getTheX = x
    def getTheY = y

    // Gestion des icones
    def chgIconEtEnable(im: ImageIcon, en: Boolean) = {
       icon = im
       enabled = en
       disabledIcon = im
    }
   
    // Gestion des éléments du Démineur
    private var contenu = 0 // 0-8 nbre mines, 9 mine
    private var libre = true //case sans rien
    private var drapeau = false
    
    def estLibre = libre
    def chgLibre = libre = !libre
    
    def possedeDrapeau = drapeau
    def chgDrapeau = drapeau = !drapeau
    
    def setContenu(n:Int) = contenu = n
    def getContenu = contenu
    def incrContenu = contenu += 1
    
    def mines = contenu == 9
    
    def reset = {
       libre = true
       drapeau = false
   }
    

   // Gestion de la couleur du bouton
    private var couleur = Color.gray 
    private var colore = false
 
    def getCouleur = couleur

    def estColore = colore
    def estColore(c: Color) = colore && (couleur == c)
    
    def setColoration(b: Boolean) = {
       colore = b
    }
    def setCouleur(c: Color) = {
       couleur = c
    }

    def active_couleur() = {
       background = couleur
       colore = true
    }
    def active_couleur(c: Color) = {
       couleur = c
       background = c
       colore = true
    }
    def desactive_couleur() = {
       couleur = Color.gray
       background = couleur
       colore = false
    }
}

