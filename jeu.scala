import scala.util.Random
import swing._
import Array._
import javax.swing.ImageIcon
import scala.math._

abstract class Jeu[T<:Button] {
   // Variables de base
   protected var tailleX : Int = 9
   protected var tailleY : Int = 9
   protected var boutons : Array[Array[T]]

   // Tableaux des valeurs de base des paramètres
   protected var valeur_defaut = new Array[Int](0)
   
   // Fonctions utilitaires
   protected def map_init(f : (Int,Int) => T, a : Array[Array[T]]) = {
      for (i <- 0 to (tailleX-1))
         for (j <- 0 to (tailleY-1))
            a(i)(j) = f(i,j)
   }
   
   protected def map_apply(f : T => Unit, a : Array[Array[T]]) = {
      a.map(x => x.map(f))
   }
   
   protected def voisinDirect(x : Int, y : Int) = {
      var voisin = List[(Int,Int)]()
      if(x>0){voisin = (x-1, y) :: voisin}
      if(x<tailleX-1){voisin = (x+1, y) :: voisin}
      if(y>0){voisin = (x, y-1) :: voisin}
      if(y<tailleY-1){voisin = (x, y+1) :: voisin}

      voisin
   }

   protected def voisinEtendu(x : Int, y : Int): List[(Int,Int)] = {
      var voisin = List[(Int, Int)]()
      if(x>0){
         voisin = (x-1, y) :: voisin
         if(y>0){voisin = (x-1, y-1) :: voisin} 
         if(y<tailleY-1){voisin = (x-1, y+1) :: voisin} 
      }
      if(x<tailleX-1){
         voisin = (x+1, y) :: voisin
         if(y>0){voisin = (x+1, y-1) :: voisin} 
         if(y<tailleY-1){voisin = (x+1, y+1) :: voisin}
      } 
      if(y>0){voisin = (x, y-1) :: voisin}
      if(y<tailleY-1){voisin = (x, y+1) :: voisin};
      voisin
   }

   // Transimission d'informations
   def getTailleX = tailleX
   def getTailleY = tailleY
   def getBoutons(x: Int,y: Int) = boutons(x)(y)
   def getValeurDefaut(i: Int) = valeur_defaut(i) 
   
   // Fonctions obligatoires
   def victoire() : Boolean
   def initialise() : Unit
   def initialise(n: Int) : Unit
   def reparametrage(x: Int, y: Int, d: Int) : Unit
   def reparametrage(x: Int, y: Int, d: Int, n: Int) : Unit
   def redemarre() : Unit
   def clique_action_droit(x: Int, y: Int) : Boolean
   def clique_action_gauche(x: Int, y: Int) : Boolean

}
