import scala.util.Random
import Array._
import java.awt.Color
import javax.swing.ImageIcon
import scala.util.control.Breaks._

class Unruly extends Jeu[MonBouton] {
   valeur_defaut = Array(8,8)

   // Cotés du terrain
   tailleX = valeur_defaut(0)
   tailleY = valeur_defaut(1)

   private var nb_non_occupes = tailleX*tailleY
   
   // Difficulte
   // 0 : Facile
   // 1 : Moyen
   // 2 : Difficile
   private var difficulte = 0

   // Matrice des boutons.                                                       
   override var boutons = ofDim[MonBouton](tailleX,tailleY)                               
                                                                                  
   // Pour reparamétrer la configuration du terrain                              
   def reparametrage(x: Int, y: Int, d: Int) = {                                         
      tailleX = x                                                                
      tailleY = y 
      difficulte = 0                                                              
      boutons = ofDim[MonBouton](tailleX,tailleY)
      nb_non_occupes = tailleX*tailleY                                
   }                                                                             

   def reparametrage(x: Int, y: Int, d: Int, n: Int) = reparametrage(x,y,d)

   def redemarre() = {
      for (i <- 0 to (tailleX-1)) {
         for (j <- 0 to (tailleY-1)) {
            if (boutons(i)(j).enabled) { boutons(i)(j).desactive_couleur }
         }
      }
      nb_non_occupes = tailleX*tailleY
   }
                                                                                 
   // Fonction pour compter le nombre de cases noires ou blanches sur
   // une ligne ou une colonne. Le booléen lig indique si on regarde une
   // ligne (true) ou une colonne (false).
   def nombre_aligne(i: Int, lig: Boolean, c: Color) = {
      var rep = 0
      if (lig) {
         for (j <- 0 to (tailleY-1)) {
            if (boutons(i)(j).estColore(c)) { rep = rep + 1 }
         }
      } else {
         for (j <- 0 to (tailleX-1)) {
            if (boutons(j)(i).estColore(c)) { rep = rep + 1 }
         }
      }
      rep
   }

   // Teste si trois cases consécutives sont colorées de la même manière 
   // lorsqu'on colorie une case Renvoie true si trois cases sont ainsi
   // detectées.
   def trois(i: Int, j:Int, c: Color) = {
      var res = false
      if (i!=0 && i!=(tailleX-1)) {
         res = res || (boutons(i-1)(j).estColore(c) && boutons(i+1)(j).estColore(c))
      }
      if (j!=0 && j!=(tailleY-1)) {
         res = res || (boutons(i)(j-1).estColore(c) && boutons(i)(j+1).estColore(c))
      }
      if (i>1) {
         res = res || (boutons(i-1)(j).estColore(c) && boutons(i-2)(j).estColore(c))
      }
      if (j>1) {
         res = res || (boutons(i)(j-1).estColore(c) && boutons(i)(j-2).estColore(c))
      }
      if (i<(tailleX-2)) {
         res = res || (boutons(i+1)(j).estColore(c) && boutons(i+2)(j).estColore(c))
      }
      if (j<(tailleY-2)) {
         res = res || (boutons(i)(j+1).estColore(c) && boutons(i)(j+2).estColore(c))
      }
      res
   }

   // Admissibilité d'une position et une couleur donnée
   def compatible(x: Int, y: Int, c: Color) = {
      (!trois(x,y,c)) && (nombre_aligne(x,true,c) < tailleY/2) && (nombre_aligne(y,false,c) < tailleX/2)
   }

   // Génération d'une grille aléatoire
   def initialise : Unit = {
      for (i <- 0 to (tailleX-1)) {
         for (j <- 0 to (tailleY-1)) {
            boutons(i)(j) = new MonBouton(i,j)
            boutons(i)(j).desactive_couleur
         }
      }
      var borne = 0
      difficulte match {
         case 0 => borne =  (tailleX*tailleY)/3
         case 1 => borne = (tailleX*tailleY)/3
         case 2 => borne = (tailleX*tailleY)/3
      }
      var x = Random.nextInt(tailleX)
      var y = Random.nextInt(tailleY)
      var k = Random.nextBoolean
      var im = new ImageIcon
      var cl = Color.white
      for (i <- 0 to borne) {
         k = Random.nextBoolean
         if (k) { 
           im = new ImageIcon(getClass.getResource("blanc_inchangeable.png"))
           cl = Color.white
         }
         else {
           im = new ImageIcon(getClass.getResource("noir_inchangeable.png"))
           cl = Color.black
         }
         while (boutons(x)(y).estColore || trois(x,y,cl)) {
           x = Random.nextInt(tailleX)
           y = Random.nextInt(tailleY)
         }
         boutons(x)(y).setColoration(true)
         boutons(x)(y).setCouleur(cl)
         boutons(x)(y).chgIconEtEnable(im, false)
         nb_non_occupes = nb_non_occupes-1
      }
      if (!solveur()) { redemarre(); initialise } else { nettoyage() }
   }

   // Génératioàn aléatoire avec graine
   def initialise(n: Int) : Unit = {
      for (i <- 0 to (tailleX-1)) {
         for (j <- 0 to (tailleY-1)) {
            boutons(i)(j) = new MonBouton(i,j)
            boutons(i)(j).desactive_couleur
         }
      }
      var borne = (tailleX*tailleY)/4
      var ran = new Random(n)
      var x = ran.nextInt(tailleX)
      var y = ran.nextInt(tailleY)
      var k = ran.nextBoolean
      var im = new ImageIcon()
      var cl = Color.white
      for (i <- 0 to borne) {
         k = ran.nextBoolean 
         if (k) { 
           im = new ImageIcon(getClass.getResource("blanc_inchangeable.png"))
           cl = Color.white
         } else {
           im = new ImageIcon(getClass.getResource("noir_inchangeable.png"))
           cl = Color.black
         }
         while (boutons(x)(y).estColore || trois(x,y,cl)) {
           x = ran.nextInt(tailleX)
           y = ran.nextInt(tailleY)
         }
         boutons(x)(y).setColoration(true)
         boutons(x)(y).setCouleur(cl)
         boutons(x)(y).chgIconEtEnable(im, false)
         nb_non_occupes = nb_non_occupes-1
      }
      if (!solveur()) { redemarre(); initialise(n+1) } else { nettoyage() }
   }

   // Test de victoire
   def victoire() = {
      nb_non_occupes == 0
   }

   // Code de base d'une action
   def theAction(i: Int, j: Int, c: Color) = {
      if(boutons(i)(j).enabled){
      if (boutons(i)(j).estColore) {
         boutons(i)(j).desactive_couleur()
         nb_non_occupes += 1
      } else {
         if (compatible(i,j,c)) { 
           boutons(i)(j).setCouleur(c)
           boutons(i)(j).active_couleur()
           nb_non_occupes -= 1
         }
      }}
   }

   // Définition des conséquences d'un clic gauche de souris
   def clique_action_gauche(i: Int, j: Int) = {
      theAction(i,j,Color.black)
      println(nb_non_occupes)
      victoire()
   }

   // Définiton des conséquences d'un clic droit de souris
   def clique_action_droit(i: Int, j: Int) = {
      theAction(i,j,Color.white)
      println(nb_non_occupes)
      victoire()
   }

   // Solveur_trivial, détermine les cases dont les règles imposent la couleur.
   def sol_trivial(x: Int, y: Int) = {
      var result = false
      var probleme = false
      if(trois(x,y,Color.black)){
         if(compatible(x,y,Color.white)) { result = true; boutons(x)(y).setCouleur(Color.white); boutons(x)(y).active_couleur() }
         else { probleme = true }}
      if(!result && trois(x,y,Color.white)){
         if(compatible(x,y,Color.black)) { result = true; boutons(x)(y).setCouleur(Color.black); boutons(x)(y).active_couleur() }
         else { probleme = true }}
      if(!result && nombre_aligne(x,true,Color.black)==(tailleY/2)){
         if(compatible(x,y,Color.white)) { result = true; boutons(x)(y).setCouleur(Color.white); boutons(x)(y).active_couleur() }
         else { probleme = true }}
      if(!result && nombre_aligne(x,true,Color.white)==(tailleY/2)){
         if(compatible(x,y,Color.black)) { result = true; boutons(x)(y).setCouleur(Color.black); boutons(x)(y).active_couleur() }
         else { probleme = true }}
      if(!result && nombre_aligne(y,false,Color.black)==(tailleX/2)){
         if(compatible(x,y,Color.white)) { result = true; boutons(x)(y).setCouleur(Color.white); boutons(x)(y).active_couleur() }
         else { probleme = true }}
      if(!result && nombre_aligne(y,false,Color.white)==(tailleX/2)){
         if(compatible(x,y,Color.black)) { result = true; boutons(x)(y).setCouleur(Color.black); boutons(x)(y).active_couleur() }
         else { probleme = true }}
      if (result) {nb_non_occupes -= 1}
      (result,probleme)
   }
   
   // Vérifie les conditions trivialles après modif.
   def checkmodif(x: Int, y: Int) : Boolean = { 
      var pb = false
      var i = 0
      var j = 0
      while(i < tailleX && !pb) { 
         if(!boutons(i)(y).estColore){
            var t = sol_trivial(i,y)
            pb = t._2
            if(t._1 && !t._2){ checkmodif(i,y) }
         }
         i = i+1
      }
      while(j < tailleY && !pb) {
         if(!boutons(x)(j).estColore){
            var t = sol_trivial(x,j)
            pb = t._2
            if(t._1 && !t._2){ checkmodif(x,j) }
         }
         j = j+1
      }
      pb
   }
   
   // Solveur, détermine si une instance donnée admet une solution.
   def solveur() : Boolean = {
      for(i <- 0 to tailleX - 1){
      for(j <- 0 to tailleY - 1){
        if(!boutons(i)(j).estColore) {
           var t = sol_trivial(i,j)
           if (t._2) {return false }
           if (t._1) { if (checkmodif(i,j)) {return false } } 
        }
      }}
      if (victoire()) { return true }
      var limite = 0
      difficulte match {
         case 0 => limite = 4
         case 1 => limite = (tailleX*tailleY)/3
         case 2 => limite = (tailleX*tailleY)/3
      }
      if (nb_non_occupes > limite) { println("pas assez : interruption"); false }
      else { 
        var (m,n) = choix()
        brutal_solve(m,n)
      } 
   }
   
   // Choisit les coordonnées d'une case non colorée (on s'assurera qu'il en existe une)
   def choix() = {
      if (victoire()) { println("Absurde") }
      var i = 0
      var j = 0
      var trouve = false
      while (!trouve && i < tailleX) {
         if (!boutons(i)(j).estColore) { trouve = true }
         else {
           if (j == tailleY-1) { j = 0 ; i = i+1 } else { j = j+1 }
         }
      }
      (i,j)
   }

   // Fonction pour résoudre de façon brute la grille
   def brutal_solve(x: Int, y: Int) : Boolean = {
      var res = false
      if (compatible(x,y,Color.black)) {
         boutons(x)(y).setCouleur(Color.black)
         boutons(x)(y).active_couleur()
         nb_non_occupes -= 1
         checkmodif(x,y)
         if (!victoire()) {
            var (m,n) = choix()
            res = brutal_solve(m,n)
         } else { res = true }
      }
      if (!res && compatible(x,y,Color.white)) {
         boutons(x)(y).setCouleur(Color.white)
         boutons(x)(y).active_couleur()
         nb_non_occupes -= 1
         checkmodif(x,y)
         if (!victoire()) {
            var (m,n) = choix()
            res = brutal_solve(m,n)
         } else { res = true }
      }
      res
   }   

   // Rétablit la grille initialement traitée par le solveur
   def nettoyage() = {
      println("Début nettoyage : "+nb_non_occupes)
      for (i <- 0 to tailleX-1) {
         for (j <- 0 to tailleY-1) {
            if (boutons(i)(j).enabled) { boutons(i)(j).desactive_couleur(); nb_non_occupes += 1 }
         }
      }
      println("Fin nettoyage : "+nb_non_occupes)
   }

}
