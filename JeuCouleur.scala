import scala.util.Random
import Array._
import java.awt.Color

abstract class JeuCouleur extends Jeu[MonBouton] {
   valeur_defaut = Array(5,5)

   // Cotés du terrain.
   tailleX = valeur_defaut(0)
   tailleY = valeur_defaut(1)

   // Matrice des boutons.
   override protected var boutons = ofDim[MonBouton](tailleX,tailleY)
   
   // Sauvegarde nécessaire pour le redémarrage
   protected var sauvegardeMat = ofDim[Color](tailleX,tailleY)

   // Difficulte
   // 0 : Facile
   // 1 : Moyen
   // 2 : Difficile
   protected var difficulte = 0

   // Pour reparamétrer la configuration du terrain
   def reparametrage(x: Int, y: Int, d: Int) = {
      tailleX = x
      tailleY = y
      difficulte = d
      boutons = ofDim[MonBouton](tailleX,tailleY)
      sauvegardeMat = ofDim[Color](tailleX,tailleY)
   }

   def reparametrage(x: Int, y: Int, d: Int, n: Int) = reparametrage(x,y,d)

   def redemarre() = {
      for(i <- 0 to (tailleX-1)) {
         for(j <- 0 to (tailleY-1)) {
            boutons(i)(j).active_couleur(sauvegardeMat(i)(j))
         }
      }
   }

   def clique_action_droit(i: Int, j: Int) = false

}
