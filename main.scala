import swing._
import event._
import Array._
import java.awt.{Color, Graphics2D}
import javax.swing.ImageIcon

object CercleInvocation extends SimpleSwingApplication {
   def top = new MainFrame { cela =>
      // On empêche le redimensionnement
      resizable = false

      // Valeurs de choix de jeu :
      // 1 : Démineur
      // 2 : Flip
      // 3 : Unruly
      // 4 : Flood
      private var choix_de_jeu = 1 
      private var jeu : Jeu[MonBouton] = new Demineur

      // Convertisseur sécurisé
      private def string_to_int (s: String) = {
         try {
            s.toInt
         } catch {
            case e:Exception => -1
         }
      }

      // Boutons pour la sélection de la difficulté
      private val but_fac = new RadioButton("Facile")
      private val but_moy = new RadioButton("Moyen")
      private val but_dur = new RadioButton("Difficile")
      private val but_per = new RadioButton("Personnalisé")
      private val group = new ButtonGroup(but_fac, but_moy, but_dur, but_per)
      group.select(but_fac)

      // Boutons pour la sélection du jeu
      private val tt_dem = new RadioButton("Démineur")
      private val tt_fli = new RadioButton("Flip")
      private val tt_unr = new RadioButton("Unruly")
      private val tt_flo = new RadioButton("Flood")
      private val touslesjeux = new ButtonGroup(tt_dem, tt_fli, tt_unr, tt_flo)
      touslesjeux.select(tt_dem)

      // Base de créaition de nouveau jeu
      private def nouveau = {
         choix_de_jeu match {
            case 1 => jeu = new Demineur
            case 2 => jeu = new Flip
            case 3 => jeu = new Unruly
            case 4 => jeu = new Flood
         }
         group.selected.get match {
            case `but_fac` =>
               choix_de_jeu match {
                  case 1 => jeu.reparametrage(9,9,0,10)
                  case 2 => jeu.reparametrage(5,5,0) 
                  case 3 => jeu.reparametrage(8,8,0)
                  case 4 => jeu.reparametrage(8,10,0)
               }
            case `but_moy` =>
               choix_de_jeu match {
                  case 1 => jeu.reparametrage(16,16,1,40)
                  case 2 => jeu.reparametrage(7,7,1) 
                  case 3 => jeu.reparametrage(10,10,1)
                  case 4 => jeu.reparametrage(10,10,1)
               }
            case `but_dur` =>
               choix_de_jeu match {
                case 1 => jeu.reparametrage(16,16,2,99)
                case 2 => jeu.reparametrage(9,9,2)
                case 3 => jeu.reparametrage(14,14,2)
                case 4 => jeu.reparametrage(14,14,2)
               }
            case `but_per` => 
               choix_de_jeu match {
                  case 1 => jeu.reparametrage(diff_fr.li,diff_fr.co,1,diff_fr.aut)
                  case 2 => jeu.reparametrage(diff_fr.li,diff_fr.co,1) 
                  case 3 => jeu.reparametrage(2*(diff_fr.li/2),2*(diff_fr.co/2),1) 
                  case 4 => jeu.reparametrage(2*(diff_fr.li/2),2*(diff_fr.co/2),1) 
               }
         }
      }

      // Création d'un nouveau jeu
      private def new_game = {
         nouveau
         jeu.initialise
         contents = genContents
         listen
      }

      // Activation des surveillances de boutons.
      private def listen = 
         for (i <- 0 to (jeu.getTailleX-1)) {
            for (j <- 0 to (jeu.getTailleY-1)) {
               listenTo(jeu.getBoutons(i,j))
               listenTo(jeu.getBoutons(i,j).mouse.clicks)
            }
         }

      // Genesation de la grille
      private def genContents =
         new GridPanel(jeu.getTailleX,jeu.getTailleY) {
            for (i <- 0 to (jeu.getTailleX-1)) {
               for (j <- 0 to (jeu.getTailleY-1)) {
                  contents += jeu.getBoutons(i,j)
               }
            }
         }

      // Redémarrage du jeu
      private def redemarrage = {
         jeu.redemarre
         contents = genContents
         listen
      }

      // Génération à partir d'une graine aléatoire
      private def genere_a_parametre = {
         val r = Dialog.showInput(contents.head, "Entrez une graine de génération :",
                 title="Génération paramétrée", initial = "12",
                 icon = new ImageIcon(getClass.getResource("case_mine.png")))
         r match {
            case Some(s) => nouveau 
                jeu.initialise(string_to_int(s))
                contents = genContents 
                listen
            case None =>
         }
      }
      
      object text_ligne extends TextField { columns = 5 }
      object text_colonne extends TextField { columns = 5 }
      object text_autre extends TextField { columns = 5 }

      def create_panel() = { 
         if (choix_de_jeu == 1) {
            new GridPanel(3,2) {
               contents += new Label("Nombre de lignes :     ") 
               contents += text_ligne
               contents += new Label(" Nombre de colonnes : ")
               contents += text_colonne
               contents += new Label("Nombre de mines :      ")
               contents += text_autre
               border = Swing.EmptyBorder(10,20,10,20)
            }
         } else {
            new GridPanel(2,2) {
               contents += new Label("Nombre de lignes :     ") 
               contents += text_ligne
               contents += new Label(" Nombre de colonnes : ")
               contents += text_colonne
               border = Swing.EmptyBorder(10,20,10,20)
            }
         }
      }

      private var diff_fr = new Frame { ceci =>
         title = "Difficulté personnalisée"
 
         var li = 0
         var co = 0
         var aut = 0

         val sortie = new FlowPanel {
            contents += new Button(Action("Valider") {
               if (string_to_int(text_ligne.text) > 0) { li = string_to_int(text_ligne.text) } else { li = cela.jeu.getValeurDefaut(0) }
               if (string_to_int(text_colonne.text) > 0) { co = string_to_int(text_colonne.text) } else { co = cela.jeu.getValeurDefaut(1) }
               if (cela.choix_de_jeu == 1) {
                  if (string_to_int(text_autre.text) > 0) { aut = string_to_int(text_autre.text) } else { aut = cela.jeu.getValeurDefaut(2) }
               }
               cela.choix_de_jeu match {
                  case 1 => cela.jeu = new Demineur ; cela.jeu.reparametrage(li,co,1,aut)
                  case 2 => cela.jeu = new Flip ; cela.jeu.reparametrage(li,co,1)
                  case 3 => cela.jeu = new Unruly ; cela.jeu.reparametrage(2*(li/2),2*(co/2),1)
                  case 4 => cela.jeu = new Flood ; cela.jeu.reparametrage(2*(li/2),2*(co/2),1)
               }
               cela.jeu.initialise
               cela.contents = genContents
               cela.listen
               ceci.dispose()
            }) 
         }

         def regenere = {
            contents = new BoxPanel(Orientation.Vertical) { 
               contents += create_panel()
               contents += sortie
            }
         }
      }
 
      // Création d'un jeu initial.
      jeu.initialise

      // Elément de décor.
      title = "The Great Satanist Programming Project"

      // Barre de menu.
      menuBar = new MenuBar {
         contents += new Menu("Options") {
            contents += new MenuItem(Action("Nouveau jeu") {
               new_game
            })
            contents += new MenuItem(Action("Redémarrer") {
               redemarrage
            })
            contents += new MenuItem(Action("Génération paramétrée...") {
               genere_a_parametre
            })
            contents += new Menu("Difficulté") {
               contents ++= group.buttons
            }
            contents += new MenuItem(Action("Sortir"){
               sys.exit(0)
            })
         }
         contents += new Menu("Autres jeux") {
            contents ++= touslesjeux.buttons
         }
      }
    
      //contents += new Label("Il te reste 10 mines !")
       
      // Elements de la fenêtre.
      contents = genContents

      // Activation des surveillances de boutons.
      listen

      listenTo(but_fac)
      listenTo(but_moy)
      listenTo(but_dur)
      listenTo(but_per)
  
      listenTo(tt_dem)
      listenTo(tt_fli)
      listenTo(tt_unr)
      listenTo(tt_flo)

      // Réaction aux évènements.
      reactions += {
         case (e: MouseReleased) => if (e.peer.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            var i = (e.peer.getComponent.getY)/50
            var j = (e.peer.getComponent.getX)/50
            if (choix_de_jeu == 1) {
               var bo = jeu.clique_action_gauche(i,j)
               if (bo) {
                  Dialog.showMessage(contents.head, "Désolé, vous avez perdu !", "Boum !!!",
                     Dialog.Message.Info, new ImageIcon(getClass.getResource("case_mine.png")))
                  redemarrage
               } else {
                  if (jeu.victoire) {
                     Dialog.showMessage(contents.head, "Félicitations, vous avez gagné !", "Victoire !!!",
                        Dialog.Message.Info, new ImageIcon(getClass.getResource("case_drapeau.png")))
                     new_game
                  }  
               }
            }
            if (choix_de_jeu == 2) {
               var bo = jeu.clique_action_gauche(i,j)
               if (jeu.victoire) {
                  Dialog.showMessage(contents.head, "Félicitations, vous avez gagné !", "Victoire !!!",
                     Dialog.Message.Info, new ImageIcon(getClass.getResource("case_drapeau.png")))
                  new_game
               }
            }
            if (choix_de_jeu == 3) {
               if (jeu.clique_action_gauche(i,j)) {
                  Dialog.showMessage(contents.head, "Félicitations, vous avez gagné !", "Bravo !!!",
                     Dialog.Message.Info, new ImageIcon(getClass.getResource("blanc_inchangeable.png")))
                  new_game
               }
            }
            if (choix_de_jeu == 4) {
               var bo = jeu.clique_action_gauche(i,j)
               if (jeu.victoire) {
                  Dialog.showMessage(contents.head, "Félicitations, vous avez gagné !", "Victoire !!!",
                     Dialog.Message.Info, new ImageIcon(getClass.getResource("case_drapeau.png")))
                  new_game
               }
            }
         }
         if (e.peer.getButton() == java.awt.event.MouseEvent.BUTTON3) {
            var i = (e.peer.getComponent.getY)/50
            var j = (e.peer.getComponent.getX)/50
            if (choix_de_jeu == 1) {
               var bo = jeu.clique_action_droit(i,j)
               if (bo) {
                  Dialog.showMessage(contents.head, "Félicitations, vous avez gagné !", "Victoire !!!",
                     Dialog.Message.Info, new ImageIcon(getClass.getResource("case_drapeau.png")))
                  new_game
               }
            }
            if (choix_de_jeu == 3) {
               if (jeu.clique_action_droit(i,j)) {
                  Dialog.showMessage(contents.head, "Félicitations, vous avez gagné !", "Bravo !!!",
                     Dialog.Message.Info, new ImageIcon(getClass.getResource("noir_inchangeable.png")))
                  new_game
               }   
            }
         }
         case ButtonClicked(comp) if (comp == but_fac) || (comp == but_moy) || (comp == but_dur) => new_game
         case ButtonClicked(comp) if comp == but_per => diff_fr.regenere ; diff_fr.open
         case ButtonClicked(comp) if (comp == tt_dem) => choix_de_jeu = 1 ; new_game
         case ButtonClicked(comp) if (comp == tt_fli) => choix_de_jeu = 2 ; new_game
         case ButtonClicked(comp) if (comp == tt_unr) => choix_de_jeu = 3 ; new_game
         case ButtonClicked(comp) if (comp == tt_flo) => choix_de_jeu = 4 ; new_game
   }
}

}
